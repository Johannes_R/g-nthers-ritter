#pragma once
class Ritter : Mensch
{
private:
	int Kampfkraft;
	string Rüstungsfarbe;
public:
	int retten();
	Ritter(string name, int lebenskraft, int kampfkraft) : Mensch(name, lebenskraft) {
		Kampfkraft = kampfkraft;
	}
};