#pragma once
class Tier
{
private:
	string Name;
	int Lebenskraft;
	int Kampfkraft;
	bool Gesinnung;

public:
	Tier(string name, int lebenskraft, int kampfkraft, bool gesinnung) {
		Name = name;
		Lebenskraft = lebenskraft;
		Kampfkraft = kampfkraft;
		Gesinnung = gesinnung;
	}
};

